from django.test import TestCase


# UNIT TEST
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import tugas8
from django.http import HttpRequest
import unittest
from django.conf import settings
from importlib import import_module
from django.utils import timezone



#FUNCTIONAL TEST
from selenium import webdriver
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.common.keys import Keys 
import time

# URL TEST
class UnitTest(TestCase):
  def test_story8_exist(self):
      response = Client().get('/tugas8/')
      self.assertEqual(response.status_code , 200)
  def test_url_not_found(self):
      response = Client().get('/test')
      self.assertEqual(response.status_code, 404)

  # FUNCTION TEST
  def test_tugas8_using_index_function(self):
      found = resolve("/tugas8/")
      self.assertEqual(found.func , tugas8)

  # TEMPLATE USE TEST
  def test_tugas8_javaScript(self):
      response = Client().get('/tugas8/')
      self.assertTemplateUsed(response, 'pages/snipsets/javascript.html')

  #TEXT TEST
  def test_tugas8_content_has_content(self):
    request = HttpRequest()
    engine = import_module(settings.SESSION_ENGINE)
    request_session = engine.SessionStore(None)
    response = tugas8(request)
    html_response = response.content.decode('utf8')
    self.assertIn("Books Finder", html_response) 
  