from django.test import TestCase


# UNIT TEST
from django.test import TestCase
from django.test import Client
from django.urls import resolve,reverse
from .views import *
from django.http import HttpRequest
import unittest
from django.conf import settings
from importlib import import_module
from django.utils import timezone
from django.contrib.auth.models import User


#FUNCTIONAL TEST
from selenium import webdriver
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.common.keys import Keys 
import time

# URL TEST
class UnitTest(TestCase):
    def test_story9_exist(self):
        response = Client().get('/tugas9/')
        self.assertEqual(response.status_code , 200) 
    
    # FUNCTION TEST
    def test_tugas9_using_index_function(self):
        found = resolve("/tugas9/")
        self.assertEqual(found.func , tugas9)
    def test_tugas9_using_index_function(self):
        found = resolve("/homepage/")
        self.assertEqual(found.func , homepage)

    # TEMPLATE USER TEST
    def test_tugas_9_uses_template(self):
        response = Client().get('/tugas9/')
        self.assertTemplateUsed(response,'pages/tugas9.html') 
    
    # TEXT TEST
    def test_tugas_9_content_has_content(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = tugas9(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Welcome to Login Page", html_response)

    # TEST FOR LOGIN 
    def test_login(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='bambang', password='12345')
        self.assertTrue(logged_in, True)

    def test_page_after_login(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='bambang', password='12345')

        response = c.get('/homepage/')
        self.assertTemplateUsed(response, 'pages/homepage.html')
   
    def test_request_method(self):
        user = User.objects.create(username='bambang')
        user.set_password('12345')
        user.save()

        c = Client()
        # logged_in = c.login(username='bambang', password='12345')

        response = self.client.post(reverse('tugas9'), {'login' : 'bambang', 'password' : '12345'})