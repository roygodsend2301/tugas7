from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('tugas9/', views.tugas9, name="tugas9"),
    path('homepage/', views.homepage, name="homepage"),
]