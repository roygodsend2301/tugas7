from django.shortcuts import render,redirect

from django.contrib.auth import authenticate,login,logout
# Create your views here.

def tugas9(request):
  if request.method == "POST":
    username = request.POST['login']
    password = request.POST['password']
    user = authenticate(request = request, username = username,password =password)
    if user is not None:
      login(request,user)
      return redirect('homepage')
    
    else :
      return redirect('tugas9')

  return render(request, 'pages/tugas9.html')

def homepage(request):
  if request.method == "POST":
    if request.POST["logout"] == "Log Out":
      logout(request)
      return redirect('tugas9')
  if request.user.is_authenticated:
    return render(request, 'pages/homepage.html')
  else:
    return redirect('tugas9')