from django.test import TestCase


# UNIT TEST
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
import unittest
from django.conf import settings
from importlib import import_module
from django.utils import timezone



#FUNCTIONAL TEST
from selenium import webdriver
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.common.keys import Keys 
import time

# URL TEST
class UnitTest(TestCase):
  def test_landing_page_exist(self):
      response = Client().get('/')
      self.assertEqual(response.status_code , 200)

  def test_url_not_found(self):
      response = Client().get('/test')
      self.assertEqual(response.status_code, 404)

  # FUNCTION TEST
  def test_landing_page_using_index_function(self):
      found = resolve("/")
      self.assertEqual(found.func , index)

  # TEMPLATE USE TEST
  def test_landing_page_use_navbar(self):
      response = Client().get('/')
      self.assertTemplateUsed(response, 'pages/snipsets/navbar.html')
  
  def test_landing_page_use_javaScript(self):
      response = Client().get('/')
      self.assertTemplateUsed(response, 'pages/snipsets/javascript.html')

  #TEXT TEST
  def test_landing_page_content_has_content(self):
    request = HttpRequest()
    engine = import_module(settings.SESSION_ENGINE)
    request_session = engine.SessionStore(None)
    response = index(request)
    html_response = response.content.decode('utf8')
    self.assertIn("Activity", html_response)
  
  def test_landing_page_content_has_content1(self):
    request = HttpRequest()
    engine = import_module(settings.SESSION_ENGINE)
    request_session = engine.SessionStore(None)
    response = index(request)
    html_response = response.content.decode('utf8')
    self.assertIn("Organization / Volunteering", html_response)

  def test_landing_page_content_has_content2(self):
    request = HttpRequest()
    engine = import_module(settings.SESSION_ENGINE)
    request_session = engine.SessionStore(None)
    response = index(request)
    html_response = response.content.decode('utf8')
    self.assertIn("Achievement", html_response)

