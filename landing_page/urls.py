from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'landing_page'

urlpatterns = [
    path('', views.index, name="home"),
]