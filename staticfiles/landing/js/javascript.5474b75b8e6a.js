$("#darkmode-switch").on("click", () => {
  if ($("#darkmode-switch").is(":checked")) {
      $("body").css("background-color", "black");
      
      $(".caption").css("color","white")

      $(".text-bg-color").css("color", "white");
      $("#day-night-txt").text("Night Mode");

      $("#navbar-color").css("background-color", "#333B43");
      $("#navbar-title").addClass("crimson-txt");
      $(".navbar-text").css("color", "#E9F4FF");

      $(".accord-header").addClass("crimson-bg");
  } else {
      $("body").css("background-color", "white");

      $("#day-night-txt").text("Day Mode");

      $("#navbar-color").css("background-color", "#E9F4FF");
      $("#navbar-title").removeClass("crimson-txt");
      $(".navbar-text").css("color", "#333B43");

      $(".caption").css("color","black")
      $(".text-bg-color").css("color", "black");

      $(".accord-header").removeClass("crimson-bg");
  }
});

$( () => {
  $( "#accordion" ).accordion(
    {
      collapsible :true
    }
  );
});
