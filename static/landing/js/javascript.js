$(document).ready(init());


function init(){
  $.ajax({
    url : "https://www.googleapis.com/books/v1/volumes?q=django" ,
    dataType : "json",
    
    success : function(data){
      console.log(data)
      for(var i =0 ; i < data.items.length ; i++){
        var title = data.items[i].volumeInfo.title
        var photo = data.items[i].volumeInfo.imageLinks.thumbnail
        var author = data.items[i].volumeInfo.authors

        if(title == null){
          title = "tidak ada"
        }

        if(photo == null){
          photo = "tidak ada"
        }

        if(author ==null){
          author = "tidak ada"
        }
        results.innerHTML += '<div class = "card" >'+
        '<img src = "' + photo  + '" class = "card-img-top" id = "gambarBuku"> <div class = "card-body">' +'<h5 class ="card-title">' + title + '</h5>'+
        '<p class = "card-text"><small class = "text-muted">' + author + '</small></p>' + '</div> </div>'
      }
    }, 

    type: 'GET',
  });
};

$("#darkmode-switch").on("click", () => {
  if ($("#darkmode-switch").is(":checked")) {
      $("body").css("background-color", "black");
      
      $(".caption").css("color","white")

      $(".text-bg-color").css("color", "white");
      $("#day-night-txt").text("Night Mode");

      $("#navbar-color").css("background-color", "#333B43");
      $("#navbar-title").addClass("crimson-txt");
      $(".navbar-text").css("color", "#E9F4FF");

      $(".accord-header").addClass("crimson-bg");
  } else {
      $("body").css("background-color", "white");

      $("#day-night-txt").text("Day Mode");

      $("#navbar-color").css("background-color", "#E9F4FF");
      $("#navbar-title").removeClass("crimson-txt");
      $(".navbar-text").css("color", "#333B43");

      $(".caption").css("color","black")
      $(".text-bg-color").css("color", "black");

      $(".accord-header").removeClass("crimson-bg");
  }
});

$( () => {
  $( "#accordion" ).accordion(
    {
      collapsible :true
    }
  );
});



$("#button").click(function(){
  $("#results").empty()
  var search = $("#search").val();
  $.ajax({
    url : "https://www.googleapis.com/books/v1/volumes?q=" +search,
    dataType : "json",
    
    success : function(data){
      try{
        console.log(data)
        for(var i =0 ; i < data.items.length ; i++){
          var title = data.items[i].volumeInfo.title
          var photo = data.items[i].volumeInfo.imageLinks.thumbnail
          var author = data.items[i].volumeInfo.authors

          if(title == null){
            title = "tidak ada"
          }

          if(photo == null){
            photo = "tidak ada"
          }

          if(author ==null){
            author = "tidak ada"
          }
          results.innerHTML += '<div class = "card" >'+
          '<img src = "' + photo  + '" class = "card-img-top" id = "gambarBuku"> <div class = "card-body">' +'<h5 class ="card-title">' + title + '</h5>'+
          '<p class = "card-text"><small class = "text-muted">' + author + '</small></p>' + '</div> </div>'
        }
      }
      catch{
        alert("File tidak lengkap")
        if($("#results").empty()){
          init();
        }
      }
    }, 

    type: 'GET',
  });
});

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
    ajaxStop: function() { $body.removeClass("loading"); }    
});